import os, sys

#info le tableau est en [ligne][colonne] soit [y][x]

global temptab

class Cell:

    #Initialisation d'une cellule
    def __init__(self, line, column):
        self.line = line
        self.column = column
        self.number = 0
        self.possibility = [1,2,3,4,5,6,7,8,9]
        self.euristique = 24

    #Inscription d'un nombre dans la cellule
    def SetNumber(self, i):
        if i != 0:
            self.number = i
            self.possibility = [i]
            Updatecolumn(self.line,self.column,i)
            UpdateLine(self.line,self.column,i)
            UpdateSquare(self.line,self.column,i)

    #Supression d'une possibilité dans la cellule
    def RemovePossibility(self, i): #return 1 si valeur unique, 2 si erreur, zero si rien
        if i in self.possibility:
            self.possibility.remove(i)
            self.euristique -=1
        if (self.possibility.count == 1):
            self.number = self.possibility[0]
            return 1
        elif(self.possibility.count == 0):
            return 2
        return 0

    
#Initialisation d'un sudoku à partir d'un fichier
def InitSudoku(file = "sudoku"):

    path = os.path.dirname(sys.argv[0]) #Récupération du chemin du sudoku
    try:
        # recuperation du fichier contenant le sudoku
        f = open(path+"/Sudoku/"+file +".txt", "r")
        i = 0
        for x in f:
            j = 0
            x.split()
            for y in x:
                if(y != " "):
                    if(y != '\n'):
                        #attribution des nombres dans chaque cellule
                        tab[i][j].SetNumber(int(y))
                        j += 1
            i += 1
        #fermeture du fichier contenant le sudoku
        f.close()
    except IOError:
        print("Could not read file "+ file+".\n")

#met à jour les possibilités dans une colonne concernant le number en cours
def Updatecolumn(line, column, number):
    #print("update de la colonne " + str(column))
    for _line in range(9):
        if(_line != line):
            #pour chaque cellule dans la colonne de la cellule désignée on retire la valeur number du domaine
            tab[_line][column].RemovePossibility(number)
            #print("["+str(_line)+"]["+str(column)+"] = " + str(tab[_line][column].number))

#met à jour les possibilités dans une ligne concernant le number en cours
def UpdateLine(line, column, number):
    #print("update de la ligne " + str(ligne))
    for _column in range(9):
        if(_column != column):
            #pour chaque cellule dans la ligne de la cellule désignée on retire la valeur number du domaine
            tab[line][_column].RemovePossibility(number)
            #print("["+str(line)+"]["+str(_column)+"] = " + str(tab[line][_column].number))

#met à jour les possibilités dans un carré concernant le number en cours
def UpdateSquare(line, column, number):
#pour chaque cellule dans le carre de la cellule designée on retire la valeur number du domaine
    if(line<3):
        if(column<3):
            #print("update de la square up left"))
            for _line in range(3):
                #p = str()
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)

        elif(column<6):
            #print("update de la square up midle"))
            for _line in range(3):
                #p = str()
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)
        else:
            #print("update de la square up right"))
            for _line in range(3):
                #p = str()
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)
    elif(line<6):
            #print("update de la square midle left"))
        if(column<3):
            for _line in range(3,6):
                #p = str()
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)

        elif(column<6):
            #print("update de la square in the midle"))
            for _line in range(3,6):
                #p = str()
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)
        else:
            #print("update de la square midle right"))
            for _line in range(3,6):
                #p = str()
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)
    else:
        if(column<3):
            #print("update de la square lower left"))
            for _line in range(6,9):
                #p = str()
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)

        elif(column<6):
            #print("update de la square lower midle"))
            for _line in range(6,9):
                #p = str()
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)
        else:
            #print("update de la square lower right"))
            for _line in range(6,9):
                #p = str()
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        tab[_line][_column].RemovePossibility(number)
                        #p += str(tab[_line][_column].number) + " ";
                #print(p)
    pass

# retourne true si une autre case dans la colonne peut prendre la valeur number
def columnHasPossibility(line, column, number):
    for _line in range(9):
        if(_line != line):
            # on ne teste que les cases non remplies
            if tab[_line][column].number == 0:
                if number in tab[_line][column].possibility:
                    #si au moins une des cases non remplies peut contenir number on s'arrete
                    return True
    return False

# retourne true si une autre case dans la ligne peut prendre la valeur number
def LineHasPossibility(line, column, number):
    for _column in range(9):
        if(_column != column):
            # on ne teste que les case non remplies
            if tab[line][_column].number == 0:
                if number in tab[line][_column].possibility:
                    #si au moins une des cases non remplies peut contenir number on s'arrete
                    return True
    return False

#return true si une autre case dans le carré peut prendre la valeur number
def SquareHasPossibility(line, column, number):
    if(line<3):
        if(column<3):
            #print("update de la square up left"))
            for _line in range(3):
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des cases non remplies peut contenir number, on s'arrete
                                return True

        elif(column<6):
            #print("update de la square up midle"))
            for _line in range(3):
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des cases non remplies peut contenir number, on s'arrete
                                return True
        else:
            #print("update de la square up right"))
            for _line in range(3):
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des cases non remplies peut contenir number, on s'arrete
                                return True
    elif(line<6):
            #print("update de la square midle left"))
        if(column<3):
            for _line in range(3,6):
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des case non remplies peut contenir number on s'arrete
                                return True

        elif(column<6):
            #print("update de la square in the midle"))
            for _line in range(3,6):
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des cases non remplies peut contenir number on s'arrete
                                return True
        else:
            #print("update de la square midle right"))
            for _line in range(3,6):
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des cases non remplies peut contenir number on s'arrete
                                return True
    else:
        if(column<3):
            #print("update de la square lower left"))
            for _line in range(6,9):
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des cases non remplies peut contenir number on s'arrete
                                return True

        elif(column<6):
            #print("update de la square lower midle"))
            for _line in range(6,9):
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des cases non remplies peut contenir number on s'arrete
                                return True
        else:
            #print("update de la square lower right"))
            for _line in range(6,9):
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == 0:
                            if number in tab[_line][_column].possibility:
                                #si au moins une des cases non remplies peut contenir number on s'arrete
                                return True
    return False

# retourne true si une autre case dans la colonne possède la valeur number
def columnHasNumber(line, column, number):
    for _line in range(9):
        if(_line != line):
            if tab[_line][column].number == number:
                    return True
    return False

# retourne true si une autre case dans la ligne possède la valeur number
def LineHasNumber(line, column, number):
    for _column in range(9):
        if(_column != column):
            if tab[line][_column].number == number:
                return True
    return False

def SquareHasNumber(line, column, number):
    if(line<3):
        if(column<3):
            #print("update de la square up left"))
            for _line in range(3):
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True

        elif(column<6):
            #print("update de la square up midle"))
            for _line in range(3):
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True
        else:
            #print("update de la square up right"))
            for _line in range(3):
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True
    elif(line<6):
            #print("update de la square midle left"))
        if(column<3):
            for _line in range(3,6):
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True

        elif(column<6):
            #print("update de la square in the midle"))
            for _line in range(3,6):
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True
        else:
            #print("update de la square midle right"))
            for _line in range(3,6):
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True
    else:
        if(column<3):
            #print("update de la square lower left"))
            for _line in range(6,9):
                for _column in range(3):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True

        elif(column<6):
            #print("update de la square lower midle"))
            for _line in range(6,9):
                for _column in range(3,6):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True
        else:
            #print("update de la square lower right"))
            for _line in range(6,9):
                for _column in range(6,9):
                    if ((line != _line) or (column != _column)):
                        if tab[_line][_column].number == number:
                            return True
    return False


def isPossible(line, column, number):
    if(LineHasNumber(line, column, number)):
        return False
    if(columnHasNumber(line, column, number)):
        return False
    if(SquareHasNumber(line, column, number)):
        return False
    return True


#return 1 si une case est remplie.
def SetNumberInCell(tab, line, column, num):
    if not (columnHasPossibility(line, column, num)):
        tab[line][column].SetNumber(num)
        #print("["+str(line)+"]["+ str(column) + "] column")
        return 1
    if not (LineHasPossibility(line, column, num)):
        tab[line][column].SetNumber(num)
        #print(str(line)+":"+ str(column) + "line")
        return 1
    if not (SquareHasPossibility(line, column, num)):
        tab[line][column].SetNumber(num)
        #print(str(line)+":"+ str(column) + "square")
        return 1
    return 0


# affiche le sudoku
def printTab(tab):
    for y in range(9):
        myString = str(" ")
        for x in range(9):
            if(x%3):
                pass
            elif(x != 0):
                myString += "| "
            if tab[y][x].number == 0:
                myString += "  "
            else:
                myString += str(tab[y][x].number) + " "
        
        if(y%3):
            pass
        elif(y != 0):
            print(" - - -   - - -   - - - ")
        print(myString)

#partie résolution de sudoku

def UpdateTest(masterI):
    global nbvides
    global temptab
    nbvides = 0
    temptab = []
    #On remplit les cases évidentes (celles qui sont vides avec une seule possibilité restante)
    for x in range(1,9): #Minimum remaining value pour chacune des cellules.
        for line in range(9):
            for column in range(9):
                #Si la case est vide 
                if (tab[line][column].number == 0) :
                    nbvides =nbvides+1
                    if (len(tab[line][column].possibility) == 1):
                        #print("evident",x, len(tab[line][column].possibility))
                        #on remplit d'abord les possibilités évidentes
                        tab[line][column].SetNumber(tab[line][column].possibility[0])
                        return 1
                    #Si une case a plusieurs possibilités, on vérifie si l'une n'est pas évidente.
                    elif (len(tab[line][column].possibility) == x):
                        #print("Set Number",x, len(tab[line][column].possibility))
                        for num in tab[line][column].possibility:
                            masterI=SetNumberInCell(tab, line, column, num)
                            if (masterI!=0):
                                return masterI
                        temptab.append(tab[line][column])
    nbvides = int(nbvides/8)
    return masterI


def reccursiveBactracking(BTtab):
    #print
    #
    #print("reccursive bactracking len(temptab) =", len(temptab))
    if (len(temptab) == 0): 
        return BTtab   
    BTcell = temptab.pop(len(temptab)-1)
    #print
    #print("vides restantes : ", len(temptab))
    #print("BTcell", BTcell.line, BTcell.column)
    for num in BTcell.possibility:
        #print
        #print("BTcell number", BTcell.line, BTcell.column, num)
        if isPossible(BTcell.line, BTcell.column, num):
            #print
            #print("BTcell number", BTcell.line, BTcell.column, num, "set")
            BTcell.number = num
            if(reccursiveBactracking(BTtab)):
                return True
            BTcell.number = 0
    temptab.append(BTcell)
    return False
        

def backtrackingSearch():
    BTtab = tab
    print("backtracking en cours")
    reccursiveBactracking(BTtab)
    return BTtab

    
#region Programme
i=0
while(i==0):

    tab = []
    # init tableau de cell
    for line in range(9):
        tmp = []
        for column in range(9):
            tmp.append(Cell(line, column))
        tab.append(tmp)

    # recuperation du bon sudoku
    n =input("Enter file name: \n")
    if(n == ""):
        InitSudoku()
    else:
        InitSudoku(n)


    printTab(tab)

    #resolution sudoku
    #pour le moment on ne cherche que les éléments qui n'ont plus qu'une variable dans leur domaine
    masterI=1
    while masterI!=0:
        masterI = UpdateTest(0)
    print(nbvides)

    # ordonner temptab avec degree euristique et mrv
    mrvTab = []
    # On ordonne temptab en fonction du MRV (des possibilités du plus petit au plus grand)
    for x in range(2,9):
        for i in range (len(temptab)):
            if len(temptab[i].possibility) == x:
                mrvTab.append(temptab[i])
    degreeTab = []

    #On ordonne les possibilités du plus petit au plus grand en fonction des euristiques (du plus grand au plus petit)
    for i in range(len(mrvTab)):
        for j in range(i+1,len(mrvTab)):
            if mrvTab[i].euristique<=mrvTab[j].euristique and len(mrvTab[i].possibility) == len(mrvTab[j].possibility):
                mrvTab[i], mrvTab[j] = mrvTab[j], mrvTab[i]
        degreeTab.append(mrvTab[i])

    #On réactuaise temptab avec la liste ordonnée
    for i in range(len(degreeTab)):
        temptab[i] = degreeTab[i]

    tabfin = backtrackingSearch()

    print("\n---------------------- \n")
    printTab(tabfin)
    print('\n')

#endregion