CSP_Sudoku




Utilisation du programme :

Prérequis :
-Un sudoku enregistré dans un fichier .txt suivant ce template :

0 2 0 1 7 0 5 0 3
0 5 0 0 0 0 0 2 7
0 0 0 6 5 0 0 9 0
2 1 0 4 0 7 0 3 0
7 3 4 8 0 0 0 0 1
0 0 5 0 3 1 0 0 0
0 0 8 7 0 4 0 1 9
3 0 2 0 0 0 7 8 0
1 9 0 0 8 6 0 5 0

les 0 représentent les cases vides. Les espaces séparent les chiffres d'une même colonne.

Le fichier doit être enregistré dans le répertoire Sudoku/. Ce répertoire doit se trouver au même endroit que le script python.
Des sudoku exemples sont fournis.

Execution du programme :

-lancer le script python
-saisir le nom du fichier (sans le path ni l'extension !)
Pour ouvrir le fichier "sudoku.txt", il suffit d'écrire "sudoku".
